# README

# This is a testing project.

# It simulates the FB mobile UI, including side bar, pull down to refresh 10 new data, and pull up to the end to load 10 old data. (The 10 data will be the same whenever refreshed/loaded)

# Reference:
# PullToRefreshListView.java gives much credit to Erik Wallentinsen.
# His source code could be found on https://github.com/erikwt/PullToRefresh-ListView.
# Another source code https://github.com/shontauro/android-pulltorefresh-and-loadmore is also credited for the part of loading old data.

# Major existing bug: 
# 1) Refreshing sign will cover the slide button.
# 2) When side bar is out, tapping the content list will cause layout errors when clicking slide button next time.

# by Chiu Po Tsung