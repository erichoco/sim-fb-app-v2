package test.sim_fb;

//import test.sim_fb.PullDownListView.OnRefreshListener;
import com.example.sim_facebook_app.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PullDownListView extends ListView {
	
    private static final float PULL_RESISTANCE                 = 1.7f;
    private static final int   BOUNCE_ANIMATION_DURATION       = 700;
    private static final int   BOUNCE_ANIMATION_DELAY          = 100;
    private static final float BOUNCE_OVERSHOOT_TENSION        = 1.4f;
    private static final int   ROTATE_ARROW_ANIMATION_DURATION = 250;
	
	private static enum State {
        PULL_TO_REFRESH,
        RELEASE_TO_REFRESH,
        REFRESHING
    }
	
	ImageView arrow;
	ProgressBar refreshCircle;
	TextView pullRelease;
	
	private String pullToRefreshText;
    private String releaseToRefreshText;
    private String refreshingText;
    
    private RotateAnimation flipAnimation;
    private RotateAnimation reverseFlipAnimation;
    
    private State state;
	
	/*public interface OnRefreshListener() {
		public void onRefresh();
	}
	
    public void setOnRefreshListener(OnRefreshListener onRefreshListener){
        this.onRefreshListener = onRefreshListener;
    }*/
	
	public PullDownListView(Context context) {
		super(context);
		init();
	}
	
	public PullDownListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public PullDownListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		
		arrow = (ImageView)findViewById(R.id.arrow);
		refreshCircle = (ProgressBar)findViewById(R.id.spinner);
		pullRelease = (TextView)findViewById(R.id.pull_release);
		
		pullToRefreshText = getContext().getString(R.string.pull_to_refresh);
		releaseToRefreshText = getContext().getString(R.string.release_to_refresh);
		refreshingText = getContext().getString(R.string.refreshing);
		
		state = State.PULL_TO_REFRESH;
		
		flipAnimation = new RotateAnimation(0, -180, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
	    flipAnimation.setInterpolator(new LinearInterpolator());
	    flipAnimation.setDuration(ROTATE_ARROW_ANIMATION_DURATION);
	    flipAnimation.setFillAfter(true);
	     
	    reverseFlipAnimation = new RotateAnimation(-180, 0, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        reverseFlipAnimation.setInterpolator(new LinearInterpolator());
        reverseFlipAnimation.setDuration(ROTATE_ARROW_ANIMATION_DURATION);
        reverseFlipAnimation.setFillAfter(true);
        
        
		
	}


}
